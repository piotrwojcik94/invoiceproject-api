package com.example.demo.tools;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DateHelperTest {

    @Test
    public void getMonthNumberTest() {
        Assert.assertEquals(1, 1);
    }
}
