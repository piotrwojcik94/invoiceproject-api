package com.example.demo.kpirBuilder;

import com.example.demo.entity.InvoiceIncomeAndExpenses;
import com.example.demo.entity.Kpir;
import com.example.demo.entity.User;

import java.sql.Date;


public class KpirBuilder {
    private Date kpirDate;
    private InvoiceIncomeAndExpenses currentPage = new InvoiceIncomeAndExpenses();
    private InvoiceIncomeAndExpenses previousPage = new InvoiceIncomeAndExpenses();
    private InvoiceIncomeAndExpenses yearPage = new InvoiceIncomeAndExpenses();
    private User user;
    private Double taxToPay;

    public void withDate(Date kpirDate) {
        this.kpirDate = kpirDate;
    }

    public KpirBuilder withPreviousPage(InvoiceIncomeAndExpenses previousPage) {
        this.previousPage = new InvoiceIncomeAndExpenses(previousPage);
        return this;
    }

    public KpirBuilder withYearPage(InvoiceIncomeAndExpenses yearPage) {
        this.yearPage = new InvoiceIncomeAndExpenses(yearPage);
        return this;
    }

    public KpirBuilder withUser(User user) {
        this.user = user;
        return this;
    }

    public KpirBuilder withTaxToPay(Double taxToPay) {
        this.taxToPay = taxToPay;
        return this;
    }

    public Kpir build() {
        return new Kpir(kpirDate, currentPage, previousPage, yearPage, user, taxToPay);
    }
}
