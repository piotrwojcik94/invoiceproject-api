package com.example.demo.tools;

import com.example.demo.entity.InvoiceType;
import com.example.demo.entity.User;

import java.util.Date;
import java.util.List;

public class FindHelper {
    public Date startDate;
    public Date endDate;
    public User user;
    public List<InvoiceType> invoiceType;

    public FindHelper(Date startDate, Date endDate, User user, List<InvoiceType> invoiceType) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.user = user;
        this.invoiceType = invoiceType;
    }
}
