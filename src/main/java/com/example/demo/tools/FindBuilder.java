package com.example.demo.tools;

import com.example.demo.entity.InvoiceType;
import com.example.demo.entity.User;

import java.util.Date;
import java.util.List;

public class FindBuilder {
    private Date startDate;
    private Date endDate;
    private User user;
    private List<InvoiceType> invoiceType;

    public FindBuilder setDates(int year, int month) {
        this.startDate = DateHelper.getDateFormFirstDay(year, month);
        this.endDate = DateHelper.getDateToLastDay(year, month);
        return this;
    }

    public FindBuilder setDatesYear(int year) {
        this.startDate = DateHelper.getDateFormFirstDay(year, 1);
        this.endDate = DateHelper.getDateToLastDay(year, 12);
        return this;
    }

    public FindBuilder setUser(User user) {
        this.user = user;
        return this;
    }

    public FindBuilder setInvoiceType(List<InvoiceType> invoiceType) {
        this.invoiceType = invoiceType;
        return this;
    }

    public FindHelper build() {
        return new FindHelper(startDate, endDate, user, invoiceType);
    }
}
