package com.example.demo.tools;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.Date;

public class DateHelper {
    public static Date getDateFormFirstDay(int year, int month) {
        return Date.from(LocalDate.of(year, month, 1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date getDateToLastDay(int year, int month) {
        return Date.from(LocalDate.of(year, month, YearMonth.of(year, month).lengthOfMonth()).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date getDateToLastDay(Date date) {
        int year = getYearNumber(date);
        int month = getMonthNumber(date);
        return Date.from(LocalDate.of(year, month, YearMonth.of(year, month).lengthOfMonth()).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static int getMonthNumber(java.sql.Date date) {
        return new Date(date.getTime()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getMonthValue();
    }

    public static int getYearNumber(java.sql.Date date) {
        return new Date(date.getTime()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear();
    }

    public static int getMonthNumber(Date date) {
        return getMonthNumber(dateToSqlDate(date));
    }

    public static int getYearNumber(Date date) {
        return getYearNumber(dateToSqlDate(date));
    }

    public static Date getDateXMonthPrevOrLaterFromFirstDay(Date date, int numberOfMonth) {
        int month = getMonthNumber(date) + numberOfMonth;
        if (month == 13) {
            return null;
        } else {
            return Date.from(LocalDate.of(getYearNumber(date), month, 1).atStartOfDay(ZoneId.systemDefault()).toInstant());
        }
    }

    public static Date getDateXMonthPrevOrLaterToLastDay(Date date, int numberOfMonth) {
        int year = getYearNumber(date);
        int month = getMonthNumber(date) + numberOfMonth;
        return Date.from(LocalDate.of(year, month, YearMonth.of(year, month).lengthOfMonth()).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static java.sql.Date dateToSqlDate(Date date) {
        return new java.sql.Date(date.getTime());
    }
}
