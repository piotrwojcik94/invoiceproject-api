package com.example.demo.tools;

import com.example.demo.entity.InvoiceIncomeAndExpenses;
import com.example.demo.entity.Kpir;

public class KpirPageCalculator {

    public void updateYear(InvoiceIncomeAndExpenses currentYear, Kpir nextKpir) {
        InvoiceIncomeAndExpenses nextYear = nextKpir.getIncomeAndExpensesYear();
        InvoiceIncomeAndExpenses nextPage = nextKpir.getIncomeAndExpensesCurrentPage();
        nextYear.setValueGoodsAndServicesSold(currentYear.getValueGoodsAndServicesSold() + nextPage.getValueGoodsAndServicesSold());
        nextYear.setTotalIncome(currentYear.getTotalIncome() + nextPage.getTotalIncome());
        nextYear.setOtherExpenses(currentYear.getOtherExpenses() + nextPage.getOtherExpenses());
        nextYear.setTotalExpenses(currentYear.getTotalExpenses() + nextPage.getTotalExpenses());
    }

    public void updatePrevPage(InvoiceIncomeAndExpenses currentPage, InvoiceIncomeAndExpenses prevPage) {
        prevPage.setValueGoodsAndServicesSold(currentPage.getValueGoodsAndServicesSold());
        prevPage.setTotalIncome(currentPage.getTotalIncome());
        prevPage.setOtherExpenses(currentPage.getOtherExpenses());
        prevPage.setTotalExpenses(currentPage.getTotalExpenses());
    }

    public void calculateNextKpir(Kpir currentKpir, Kpir nextKpir) {
        updatePrevPage(currentKpir.getIncomeAndExpensesYear(), nextKpir.getIncomeAndExpensesPreviousPage());
        updateYear(currentKpir.getIncomeAndExpensesYear(), nextKpir);
    }
}
