package com.example.demo.service;

import com.example.demo.entity.*;
import com.example.demo.respository.InvoiceRepository;
import com.example.demo.respository.UserRepository;
import com.example.demo.security.AuthenticationManager;
import com.example.demo.tools.FindHelper;
import com.example.demo.tools.KpirPageCalculator;
import com.example.demo.kpirBuilder.KpirBuilder;
import com.example.demo.respository.KpirRepository;
import com.example.demo.tools.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class KpirServiceImpl implements KpirService {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private KpirRepository kpirRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public List<Kpir> findKpirByYear(FindHelper findHelper) {
        return kpirRepository.findKpirByYear(findHelper.startDate, findHelper.endDate, findHelper.user);
    }

    @Override
    @Transactional
    public Kpir findKpirByDate(FindHelper findHelper) {
        Kpir kpir = kpirRepository.findKpirByKpirDate(findHelper.startDate, findHelper.endDate, findHelper.user);

        if (kpir == null) {
            kpir = buildNewKpir(findHelper.startDate, findHelper.user);
        }

        return kpir;
    }

    public Kpir buildNewKpir(Date kpirDate, User user) {
        KpirBuilder builder = new KpirBuilder();
        builder.withDate(DateHelper.dateToSqlDate(kpirDate));
        Date createdCompanyDate = DateHelper.getDateFormFirstDay(2018, 8);

        if (DateHelper.getMonthNumber(kpirDate) != 1 && !kpirDate.equals(createdCompanyDate)) {
            Kpir previousKpir = kpirRepository.findKpirByKpirDate(DateHelper.getDateXMonthPrevOrLaterFromFirstDay(kpirDate, -1), DateHelper.getDateXMonthPrevOrLaterToLastDay(kpirDate, -1), user);
            if (previousKpir != null) {
                builder.withPreviousPage(previousKpir.getIncomeAndExpensesYear())
                        .withYearPage(previousKpir.getIncomeAndExpensesYear())
                        .withUser(user)
                        .withTaxToPay(previousKpir.getKpirTaxToPay());
            } else {
                return null;
            }
        }

        return builder.build();
    }

    @Override
    @Transactional
    public KpirAndInvoicesDto findKpirAndInvoices(FindHelper findHelper) {
        Kpir kpir = kpirRepository.findKpirByKpirDate(findHelper.startDate, findHelper.endDate, findHelper.user);

        if (kpir == null) {
            kpir = buildNewKpir(findHelper.startDate, findHelper.user);
        }

        List<Invoice> invoices = invoiceRepository.findAllByDeliveryAndNotInKpir(findHelper.startDate, findHelper.endDate, findHelper.user);

        return new KpirAndInvoicesDto(kpir, invoices);
    }

    @Override
    @Transactional
    public Kpir saveAndFlush(KpirDto kpirDto) {
        Kpir kpir = kpirDto.kpir;
        User user = userRepository.getUserByLogin(AuthenticationManager.getCurrentUser());
        for (TaxationForm taxationForm: user.getTaxationForms()) {
            taxationForm.setUserByTaxationFormUserRecid(user);
        }
        preRemoveInvoicesFromKpir(kpir);

        kpir.setIncomeAndExpensesCurrentPage(kpir.getIncomeAndExpensesCurrentPage());
        kpir.setIncomeAndExpensesPreviousPage(kpir.getIncomeAndExpensesPreviousPage());
        kpir.setIncomeAndExpensesYear(kpir.getIncomeAndExpensesYear());
        kpir.setKpirTaxToPay(kpir.getKpirTaxToPay());
        kpir.setUserByKpirUser(user);

        kpirRepository.save(kpir);
        for (TaxationForm taxationForm: user.getTaxationForms()) {
            taxationForm.setUserByTaxationFormUserRecid(user);
        }
        kpir.setInvoices(kpir.getInvoices());
        for (Invoice invoice: kpir.getInvoices()) {
            invoice.setKpir(kpir);
            invoice.getInvoiceIncomeAndExpenses().setInvoice(invoice);
            invoice.setUser(user);
            for(InvoiceItem item: invoice.getInvoiceItems()) {
                item.setInvoice(invoice);
            }
        }
        kpirRepository.save(kpir);

        this.updateOtherKpir(kpir);
        return kpir;
    }

    public void preRemoveInvoicesFromKpir(Kpir kpir) {
        if (kpir.getRecid() != 0) {
            for (Invoice invoice: invoiceRepository.findAllByKpir_Recid(kpir.getRecid())) {
                invoice.setKpir(null);
                invoice.setIsCost(false);
            }
        }
    }

    public void updateOtherKpir(Kpir currentKpir) {
        KpirPageCalculator calculator = new KpirPageCalculator();
        Kpir nextKpir;
        Date endDate;

        for (Date startDate = DateHelper.getDateXMonthPrevOrLaterFromFirstDay(currentKpir.getDate(),1);
             startDate != null && DateHelper.getMonthNumber(startDate) <= 12;
             startDate = DateHelper.getDateXMonthPrevOrLaterFromFirstDay(startDate,1)) {

            endDate = DateHelper.getDateToLastDay(startDate);
            nextKpir = kpirRepository.findKpirByKpirDate(startDate, endDate, currentKpir.getUserByKpirUser());

            if (nextKpir != null) {
                calculator.calculateNextKpir(currentKpir, nextKpir);
                kpirRepository.save(nextKpir);
                currentKpir = nextKpir;
            } else {
                break;
            }
        }
    }
}
