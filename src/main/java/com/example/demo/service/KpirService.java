package com.example.demo.service;

import com.example.demo.entity.Kpir;
import com.example.demo.entity.KpirAndInvoicesDto;
import com.example.demo.entity.KpirDto;
import com.example.demo.tools.FindHelper;

import java.util.List;

public interface KpirService {

    List<Kpir> findKpirByYear(FindHelper findHelper);

    Kpir findKpirByDate(FindHelper findHelper);

    Kpir saveAndFlush(KpirDto kpir);

    KpirAndInvoicesDto findKpirAndInvoices(FindHelper findHelper);
}
