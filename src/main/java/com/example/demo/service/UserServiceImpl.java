package com.example.demo.service;

import com.example.demo.entity.User;
import com.example.demo.respository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    @Transactional
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setIsAdmin(true);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void update(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setIsAdmin(true);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public User getUserByLogin(String userLogin) {
        return userRepository.getUserByLogin(userLogin);
    }

    @Override
    @Transactional
    public boolean isEmailUnique(String email) {
        return userRepository.getUserByLogin(email) == null;
    }

    @Override
    @Transactional
    public boolean isNIPUnique(String userNIP) {
        return userRepository.getUserByNip(userNIP) == null;
    }

    @Override
    @Transactional
    public User getUserByNIP(String userNIP) {
        return userRepository.getUserByNip(userNIP);
    }
}
