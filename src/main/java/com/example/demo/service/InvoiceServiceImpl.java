package com.example.demo.service;

import com.example.demo.entity.Invoice;
import com.example.demo.entity.InvoiceItem;
import com.example.demo.entity.User;
import com.example.demo.respository.InvoiceRepository;
import com.example.demo.respository.UserRepository;
import com.example.demo.security.AuthenticationManager;
import com.example.demo.tools.FindHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private ContractorService contractorService;

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public Invoice delete(int id) {
        Invoice invoice = findById(id);
        if (invoice != null)
            invoiceRepository.delete(invoice);
        return invoice;
    }

    @Override
    @Transactional
    public List<Invoice> findAllByDelivery(FindHelper findHelper) {
        return invoiceRepository.findAllByDelivery(findHelper.startDate, findHelper.endDate, findHelper.user);
    }

    @Override
    @Transactional
    public List<Invoice> findAllByDeliveryAndInvoiceType(FindHelper findHelper) {
        return invoiceRepository.findAllByDeliveryAndInvoiceType(findHelper.startDate, findHelper.endDate, findHelper.invoiceType, findHelper.user);
    }

    @Override
    @Transactional
    public List<Invoice> findAllByYear(FindHelper findHelper) {
        return invoiceRepository.findAllByYear(findHelper.startDate, findHelper.endDate, findHelper.user);
    }

    @Override
    @Transactional
    public Invoice findById(int id) {
        return invoiceRepository.findByRecid(id);
    }

    @Override
    @Transactional
    public void saveAndFlush(Invoice invoice, Collection<InvoiceItem> invoiceItems) {
        User user = userRepository.getUserByLogin(AuthenticationManager.getCurrentUser());
        contractorService.save(invoice.getContractor());
        invoice.setUser(user);
        invoice.setInvoiceItems(invoiceItems);
        for (InvoiceItem item : invoiceItems)
            item.setInvoice(invoice);
        invoiceRepository.save(invoice);
    }
}
