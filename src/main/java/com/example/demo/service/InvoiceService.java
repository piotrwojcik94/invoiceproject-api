package com.example.demo.service;

import com.example.demo.entity.Invoice;
import com.example.demo.entity.InvoiceItem;
import com.example.demo.entity.InvoiceType;
import com.example.demo.tools.FindHelper;

import java.util.Collection;
import java.util.List;

public interface InvoiceService {

    Invoice delete(int id);

    List<Invoice> findAllByDelivery(FindHelper findHelper);

    List<Invoice> findAllByDeliveryAndInvoiceType(FindHelper findHelper);

    List<Invoice> findAllByYear(FindHelper findHelper);

    Invoice findById(int id);

    void saveAndFlush(Invoice invoice, Collection<InvoiceItem> invoiceItems);
}
