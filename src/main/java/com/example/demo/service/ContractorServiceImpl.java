package com.example.demo.service;

import com.example.demo.entity.Contractor;
import com.example.demo.respository.ContractorRepository;
import com.example.demo.respository.UserRepository;
import com.example.demo.security.AuthenticationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContractorServiceImpl implements ContractorService {

    @Autowired
    ContractorRepository mContractorRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public void save(Contractor contractor) {
        mContractorRepository.save(contractor);
    }

    @Override
    @Transactional
    public Page<Contractor> getContractors(int page, int size, String filter) {
        Pageable pageable = PageRequest.of(page, size);
        return mContractorRepository.getAllByContractorUserAndNameContainingOrNipContaining(userRepository.getUserByLogin(AuthenticationManager.getCurrentUser()), filter, filter, pageable);
    }

    @Override
    @Transactional
    public void update(int id, Contractor contactor) {
        Contractor existingContractor = mContractorRepository.findById(id);
        existingContractor.setName(contactor.getName());
        existingContractor.setStreet(contactor.getStreet());
        existingContractor.setHouseNumber(contactor.getHouseNumber());
        existingContractor.setFlatNumber(contactor.getFlatNumber());
        existingContractor.setZipCode(contactor.getZipCode());
        existingContractor.setCity(contactor.getCity());
        mContractorRepository.save(existingContractor);
    }

    @Override
    @Transactional
    public List<Contractor> getSuggestions(String firstNipNumbers) {
        return mContractorRepository.getSuggestions(firstNipNumbers);
    }


}
