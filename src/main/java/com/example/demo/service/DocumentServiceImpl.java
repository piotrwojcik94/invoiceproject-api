package com.example.demo.service;

import com.example.demo.entity.Document;
import com.example.demo.respository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    @Override
    @Transactional
    public void save(Document document) {
        documentRepository.save(document);
    }

    @Override
    @Transactional
    public Document findByRecid(int recid) {
        return documentRepository.findByRecid(recid);
    }
}
