package com.example.demo.service;

import com.example.demo.entity.Document;

public interface DocumentService {
    void save(Document document);

    Document findByRecid(int recid);
}
