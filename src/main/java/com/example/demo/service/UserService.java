package com.example.demo.service;

import com.example.demo.entity.User;

public interface UserService {
    void save(User user);

    void update(User user);

    boolean isEmailUnique(String email);

    boolean isNIPUnique(String userNIP);

    User getUserByLogin(String userLogin);

    User getUserByNIP(String userNIP);
}
