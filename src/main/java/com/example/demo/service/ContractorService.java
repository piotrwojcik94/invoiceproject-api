package com.example.demo.service;

import com.example.demo.entity.Contractor;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ContractorService {

    void save(Contractor contractor);

    Page<Contractor> getContractors(int page, int size, String filter);

    void update(int id, Contractor contractor);

    List<Contractor> getSuggestions(String firstNipNumbers);
}
