package com.example.demo.entity;

import java.util.List;

public class KpirDto {
    public Kpir kpir;
    public List<Invoice> invoices;
    public InvoiceIncomeAndExpenses currentPage;
    public InvoiceIncomeAndExpenses previousPage;
    public InvoiceIncomeAndExpenses year;
}
