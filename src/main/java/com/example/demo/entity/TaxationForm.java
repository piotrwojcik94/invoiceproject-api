package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "taxation_form", schema = "invoiceapp", catalog = "")
public class TaxationForm {
    private int taxationFormId;
    private TaxationFormEnum taxationForm;
    private int taxationFormYear;
    private Integer lumpSum;
    private User userByTaxationFormUserRecid;

    @Id
    @Column(name = "taxation_form_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getTaxationFormId() {
        return taxationFormId;
    }

    public void setTaxationFormId(int taxationFormId) {
        this.taxationFormId = taxationFormId;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "taxation_form")
    public TaxationFormEnum getTaxationForm() {
        return taxationForm;
    }

    public void setTaxationForm(TaxationFormEnum taxationForm) {
        this.taxationForm = taxationForm;
    }

    @Basic
    @Column(name = "taxation_form_year")
    public int getTaxationFormYear() {
        return taxationFormYear;
    }

    public void setTaxationFormYear(int taxationFormYear) {
        this.taxationFormYear = taxationFormYear;
    }

    @Basic
    @Column(name = "lump_sum")
    public Integer getLumpSum() {
        return lumpSum;
    }

    public void setLumpSum(Integer lumpSum) {
        this.lumpSum = lumpSum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaxationForm that = (TaxationForm) o;
        return taxationFormId == that.taxationFormId && taxationForm == that.taxationForm && taxationFormYear == that.taxationFormYear && Objects.equals(lumpSum, that.lumpSum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taxationFormId, taxationForm, taxationFormYear, lumpSum);
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "taxation_form_user_recid", referencedColumnName = "user_recid", nullable = false)
    @JsonIgnore
    public User getUserByTaxationFormUserRecid() {
        return userByTaxationFormUserRecid;
    }

    public void setUserByTaxationFormUserRecid(User userByTaxationFormUserRecid) {
        this.userByTaxationFormUserRecid = userByTaxationFormUserRecid;
    }
}
