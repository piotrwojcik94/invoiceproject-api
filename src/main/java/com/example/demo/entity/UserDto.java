package com.example.demo.entity;

import java.util.Set;

public class UserDto {
    public User user;
    public Set<TaxationForm> taxationForms;
}
