package com.example.demo.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "contractor", schema = "invoiceapp")
public class Contractor {
    private int recid;
    private String name;
    private String nip;
    private String city;
    private String flatNumber;
    private String houseNumber;
    private String street;
    private String zipCode;
    private boolean privatePerson;
    private User contractorUser;
    private String regon;
    private Date createDate;

    @Id
    @Column(name = "contractor_recid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRecid() {
        return recid;
    }

    public void setRecid(int recid) {
        this.recid = recid;
    }

    @Basic
    @Column(name = "contractor_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "contractor_nip")
    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    @Basic
    @Column(name = "contractor_city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "contractor_flat_number")
    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

    @Basic
    @Column(name = "contractor_house_number")
    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    @Basic
    @Column(name = "contractor_street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "contractor_zip_code")
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Basic
    @Column(name = "contractor_private_person")
    public boolean getPrivatePerson() {
        return privatePerson;
    }

    public void setPrivatePerson(boolean privatePerson) {
        this.privatePerson = privatePerson;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contractor_user")
    public User getContractorUser() {
        return contractorUser;
    }

    public void setContractorUser(User contractorUser) {
        this.contractorUser = contractorUser;
    }

    @Basic
    @Column(name = "contractor_regon")
    public String getRegon() {
        return regon;
    }

    public void setRegon(String street) {
        this.regon = regon;
    }

    @Basic
    @Column(name = "contractor_create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contractor that = (Contractor) o;
        return recid == that.recid &&
                Objects.equals(name, that.name) &&
                Objects.equals(nip, that.nip) &&
                Objects.equals(city, that.city) &&
                Objects.equals(flatNumber, that.flatNumber) &&
                Objects.equals(houseNumber, that.houseNumber) &&
                Objects.equals(street, that.street) &&
                Objects.equals(zipCode, that.zipCode) &&
                Objects.equals(privatePerson, that.privatePerson);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recid, name, nip, city, flatNumber, houseNumber, street, zipCode);
    }
}
