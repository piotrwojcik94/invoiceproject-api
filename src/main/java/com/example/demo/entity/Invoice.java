package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "invoice", schema = "invoiceapp")
public class Invoice {
    private int recid;
    private String number;
    private Date delivery;
    private Date issue;
    private Date posting;
    private PriceType priceType;
    private Date received;
    private Date payment;
    private String releasePlace;
    private Boolean paymentDeadlineLikeOnContract;
    private Integer daysToPay;
    private String comment;
    private PaymentType paymentType;
    private Boolean payed;
    private User user;
    private Collection<InvoiceItem> invoiceItems;
    private Contractor contractor;
    private InvoiceType invoiceType;
    private Double net;
    private Double vat;
    private Double gross;
    private boolean isCost;
    private Double vatCost;
    private Double netCost;
    private Double invoiceCost;
    private Kpir kpir;
    private InvoiceIncomeAndExpenses invoiceIncomeAndExpenses;
    private Document document;

    @Id
    @Column(name = "invoice_recid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRecid() {
        return recid;
    }

    public void setRecid(int invoiceRecid) {
        this.recid = invoiceRecid;
    }

    @Basic
    @Column(name = "invoice_number")
    public String getNumber() {
        return number;
    }

    public void setNumber(String invoiceNumber) {
        this.number = invoiceNumber;
    }

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "invoice_delivery")
    public Date getDelivery() {
        return delivery;
    }

    public void setDelivery(Date invoiceDelivery) {
        this.delivery = invoiceDelivery;
    }

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "invoice_issue")
    public Date getIssue() {
        return issue;
    }

    public void setIssue(Date invoiceIssue) {
        this.issue = invoiceIssue;
    }

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "invoice_posting")
    public Date getPosting() {
        return posting;
    }

    public void setPosting(Date invoicePosting) {
        this.posting = invoicePosting;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "invoice_price_type")
    public PriceType getPriceType() {
        return priceType;
    }

    public void setPriceType(PriceType invoicePriceType) {
        this.priceType = invoicePriceType;
    }

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "invoice_received")
    public Date getReceived() {
        return received;
    }

    public void setReceived(Date invoiceReceived) {
        this.received = invoiceReceived;
    }

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "invoice_payment")
    public Date getPayment() {
        return payment;
    }

    public void setPayment(Date invoicePayment) {
        this.payment = invoicePayment;
    }

    @Basic
    @Column(name = "invoice_release_place")
    public String getReleasePlace() {
        return releasePlace;
    }

    public void setReleasePlace(String invoiceReleasePlace) {
        this.releasePlace = invoiceReleasePlace;
    }

    @Basic
    @Column(name = "invoice_payment_deadline_like_on_contract")
    public Boolean getPaymentDeadlineLikeOnContract() {
        return paymentDeadlineLikeOnContract;
    }

    public void setPaymentDeadlineLikeOnContract(Boolean invoicePaymentDeadlineLikeOnContract) {
        this.paymentDeadlineLikeOnContract = invoicePaymentDeadlineLikeOnContract;
    }

    @Basic
    @Column(name = "invoice_days_to_pay")
    public Integer getDaysToPay() {
        return daysToPay;
    }

    public void setDaysToPay(Integer invoiceDaysToPay) {
        this.daysToPay = invoiceDaysToPay;
    }

    @Basic
    @Column(name = "invoice_comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String invoiceComment) {
        this.comment = invoiceComment;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "invoice_payment_type")
    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType invoicePaymentType) {
        this.paymentType = invoicePaymentType;
    }

    @Basic
    @Column(name = "invoice_payed")
    public Boolean getPayed() {
        return payed;
    }

    public void setPayed(Boolean invoicePayed) {
        this.payed = invoicePayed;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "invoice_type")
    public InvoiceType getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(InvoiceType invoiceType) {
        this.invoiceType = invoiceType;
    }

    @Basic
    @Column(name = "invoice_net")
    public Double getNet() {
        return net;
    }

    public void setNet(Double invoiceNet) {
        this.net = invoiceNet;
    }

    @Basic
    @Column(name = "invoice_vat")
    public Double getVat() {
        return vat;
    }

    public void setVat(Double invoiceVat) {
        this.vat = invoiceVat;
    }

    @Basic
    @Column(name = "invoice_gross")
    public Double getGross() {
        return gross;
    }

    public void setGross(Double invoiceGross) {
        this.gross = invoiceGross;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invoice invoice = (Invoice) o;
        return recid == invoice.recid &&
                Objects.equals(number, invoice.number) &&
                Objects.equals(delivery, invoice.delivery) &&
                Objects.equals(issue, invoice.issue) &&
                Objects.equals(posting, invoice.posting) &&
                Objects.equals(priceType, invoice.priceType) &&
                Objects.equals(received, invoice.received) &&
                Objects.equals(payment, invoice.payment) &&
                Objects.equals(releasePlace, invoice.releasePlace) &&
                Objects.equals(paymentDeadlineLikeOnContract, invoice.paymentDeadlineLikeOnContract) &&
                Objects.equals(daysToPay, invoice.daysToPay) &&
                Objects.equals(comment, invoice.comment) &&
                Objects.equals(paymentType, invoice.paymentType) &&
                Objects.equals(payed, invoice.payed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recid, number, delivery, issue, posting, priceType, received, payment, releasePlace, paymentDeadlineLikeOnContract, daysToPay, comment, paymentType, payed);
    }

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "invoice_user", referencedColumnName = "user_recid")
    public User getUser() {
        return user;
    }

    public void setUser(User userByInvoiceUser) {
        this.user = userByInvoiceUser;
    }

    @OneToMany(mappedBy = "invoice", fetch=FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    public Collection<InvoiceItem> getInvoiceItems() {
        return invoiceItems;
    }

    public void setInvoiceItems(Collection<InvoiceItem> invoiceItemsByInvoiceRecid) {
        this.invoiceItems = invoiceItemsByInvoiceRecid;
    }

    @ManyToOne()
    @JoinColumn(name = "invoice_contractor", referencedColumnName = "contractor_recid", nullable = false)
    public Contractor getContractor() {
        return contractor;
    }

    public void setContractor(Contractor contractorByInvoiceContractor) {
        this.contractor = contractorByInvoiceContractor;
    }

    @Basic
    @Column(name = "invoice_is_cost")
    public boolean isIsCost() {
        return isCost;
    }

    public void setIsCost(boolean invoiceIsCost) {
        this.isCost = invoiceIsCost;
    }

    @Basic
    @Column(name = "invoice_vat_cost")
    public Double getVatCost() {
        return vatCost;
    }

    public void setVatCost(Double invoiceVatCost) {
        this.vatCost = invoiceVatCost;
    }

    @Basic
    @Column(name = "invoice_net_cost")
    public Double getNetCost() {
        return netCost;
    }

    public void setNetCost(Double invoiceNetCost) {
        this.netCost = invoiceNetCost;
    }

    @Basic
    @Column(name = "invoice_cost")
    public Double getInvoiceCost() {
        return invoiceCost;
    }

    public void setInvoiceCost(Double invoiceCost) {
        this.invoiceCost = invoiceCost;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "invoice_kpir", referencedColumnName = "kpir_recid")
    @JsonIgnore
    public Kpir getKpir() {
        return kpir;
    }

    public void setKpir(Kpir kpirByInvoiceKpir) {
        this.kpir = kpirByInvoiceKpir;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "invoice_income_and_expenses", referencedColumnName = "cost_recid")
    public InvoiceIncomeAndExpenses getInvoiceIncomeAndExpenses() {
        return invoiceIncomeAndExpenses;
    }

    public void setInvoiceIncomeAndExpenses(InvoiceIncomeAndExpenses invoiceIncomeAndExpensesByInvoiceIncomeAndExpenses) {
        this.invoiceIncomeAndExpenses = invoiceIncomeAndExpensesByInvoiceIncomeAndExpenses;
    }

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "invoice_pdf", referencedColumnName = "document_recid")
    public Document getDocument() {
        return document;
    }

    public void setDocument(Document documentByInvoicePdf) {
        this.document = documentByInvoicePdf;
    }
}
