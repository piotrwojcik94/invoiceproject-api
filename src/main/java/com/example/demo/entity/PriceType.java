package com.example.demo.entity;

public enum PriceType {
    net("0"), gross("1");

    private final String value;

    PriceType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
