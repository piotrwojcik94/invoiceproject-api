package com.example.demo.entity;

import java.util.List;

public class KpirAndInvoicesDto {
    public Kpir kpir;
    public List<Invoice> invoices;

    public KpirAndInvoicesDto(Kpir kpir, List<Invoice> invoices) {
        this.kpir = kpir;
        this.invoices = invoices;
    }
}
