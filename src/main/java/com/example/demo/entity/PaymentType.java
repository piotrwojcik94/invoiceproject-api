package com.example.demo.entity;

public enum PaymentType {
    cash("0"), cash_on_delivery("1"), credit_card("2"), transfer("3"), other("4");

    private String value;

    PaymentType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
