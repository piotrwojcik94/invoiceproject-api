package com.example.demo.entity;

import java.util.List;

public class InvoiceDto {
    public Invoice invoice;
    public List<InvoiceItem> invoiceItems;
}
