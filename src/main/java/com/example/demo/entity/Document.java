package com.example.demo.entity;

import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.IOException;
import java.util.Objects;

@Entity
@Table(name = "document", schema = "invoiceapp")
public class Document {
    private int recid;
    private String name;
    private byte[] file;

    @Id
    @Column(name = "document_recid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRecid() {
        return recid;
    }

    public void setRecid(int recid) {
        this.recid = recid;
    }

    @Basic
    @Column(name = "document_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Lob
    @Column(name = "document_file")
    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        return recid == document.recid &&
                Objects.equals(name, document.name);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(recid, name);
        result = 31 * result;
        return result;
    }

    public Document() {}

    public Document(MultipartFile file) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        this.setName(fileName);
        try {
            this.setFile(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
