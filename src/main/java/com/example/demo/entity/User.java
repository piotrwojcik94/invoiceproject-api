package com.example.demo.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "user", schema = "invoiceapp")
public class User {
    private int recid;
    private String login;
    private String password;
    private Boolean isAdmin;
    private String nip;
    private Date startDate;
    private Set<TaxationForm> taxationForms;

    @Id
    @Column(name = "user_recid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRecid() {
        return recid;
    }

    public void setRecid(int userRecid) {
        this.recid = userRecid;
    }

    @Basic
    @Column(name = "user_login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String userLogin) {
        this.login = userLogin;
    }

    @Basic
    @Column(name = "user_password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String userPassword) {
        this.password = userPassword;
    }

    @Basic
    @Column(name = "user_is_admin")
    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean userIsAdmin) {
        this.isAdmin = userIsAdmin;
    }

    @Basic
    @Column(name = "user_nip")
    public String getNip() {
        return nip;
    }

    public void setNip(String userNip) {
        this.nip = userNip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return recid == user.recid &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(isAdmin, user.isAdmin) &&
                Objects.equals(nip, user.nip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recid, login, password, isAdmin, nip);
    }

    @Basic
    @Column(name = "user_start_date")
    public Date getUserStartDate() {
        return startDate;
    }

    public void setUserStartDate(Date userStartDate) {
        this.startDate = userStartDate;
    }

    @OneToMany(mappedBy = "userByTaxationFormUserRecid", fetch=FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("taxationFormYear")
    public Set<TaxationForm> getTaxationForms() {
        return taxationForms;
    }

    public void setTaxationForms(Set<TaxationForm> taxationForms) {
        this.taxationForms = taxationForms;
    }
}
