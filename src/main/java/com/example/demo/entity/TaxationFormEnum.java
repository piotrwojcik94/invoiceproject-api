package com.example.demo.entity;

public enum TaxationFormEnum {
    general_rules("0"), linearly("1"), lump_sum("2");

    private final String value;

    TaxationFormEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
