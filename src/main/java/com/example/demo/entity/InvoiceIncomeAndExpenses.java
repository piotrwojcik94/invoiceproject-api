package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "invoice_income_and_expenses", schema = "invoiceapp")
public class InvoiceIncomeAndExpenses {
    private int recid;
    private String description;
    private Double valueGoodsAndServicesSold;
    private Double otherIncome;
    private Double totalIncome;
    private Double payInCash;
    private Double otherExpenses;
    private Double totalExpenses;
    private String comments;
    private Invoice invoice;

    public InvoiceIncomeAndExpenses() {
        this.valueGoodsAndServicesSold = 0.0;
        this.otherIncome = 0.0;
        this.totalIncome = 0.0;
        this.payInCash = 0.0;
        this.otherExpenses = 0.0;
        this.totalExpenses = 0.0;
    }

    public InvoiceIncomeAndExpenses(InvoiceIncomeAndExpenses invoiceIncomeAndExpenses) {
        this.valueGoodsAndServicesSold = invoiceIncomeAndExpenses.valueGoodsAndServicesSold;
        this.otherIncome = invoiceIncomeAndExpenses.otherIncome;
        this.totalIncome = invoiceIncomeAndExpenses.totalIncome;
        this.payInCash = invoiceIncomeAndExpenses.payInCash;
        this.otherExpenses = invoiceIncomeAndExpenses.otherExpenses;
        this.totalExpenses = invoiceIncomeAndExpenses.totalExpenses;
    }

    @Id
    @Column(name = "cost_recid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRecid() {
        return recid;
    }

    public void setRecid(int costRecid) {
        this.recid = costRecid;
    }

    @Basic
    @Column(name = "invoice_description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String invoiceDescription) {
        this.description = invoiceDescription;
    }

    @Basic
    @Column(name = "invoice_value_goods_and_services_sold")
    public Double getValueGoodsAndServicesSold() {
        return valueGoodsAndServicesSold;
    }

    public void setValueGoodsAndServicesSold(Double invoiceValueGoodsAndServicesSold) {
        this.valueGoodsAndServicesSold = invoiceValueGoodsAndServicesSold;
    }

    @Basic
    @Column(name = "invoice_other_income")
    public Double getOtherIncome() {
        return otherIncome;
    }

    public void setOtherIncome(Double invoiceOtherIncome) {
        this.otherIncome = invoiceOtherIncome;
    }

    @Basic
    @Column(name = "invoice_total_income")
    public Double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(Double invoiceTotalIncome) {
        this.totalIncome = invoiceTotalIncome;
    }

    @Basic
    @Column(name = "invoice_pay_in_cash")
    public Double getPayInCash() {
        return payInCash;
    }

    public void setPayInCash(Double invoicePayInCash) {
        this.payInCash = invoicePayInCash;
    }

    @Basic
    @Column(name = "invoice_other_expenses")
    public Double getOtherExpenses() {
        return otherExpenses;
    }

    public void setOtherExpenses(Double invoiceOtherExpenses) {
        this.otherExpenses = invoiceOtherExpenses;
    }

    @Basic
    @Column(name = "invoice_total_expenses")
    public Double getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(Double invoiceTotalExpenses) {
        this.totalExpenses = invoiceTotalExpenses;
    }

    @Basic
    @Column(name = "invoice_comments")
    public String getComments() {
        return comments;
    }

    public void setComments(String invoiceComments) {
        this.comments = invoiceComments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvoiceIncomeAndExpenses that = (InvoiceIncomeAndExpenses) o;
        return recid == that.recid &&
                Objects.equals(description, that.description) &&
                Objects.equals(valueGoodsAndServicesSold, that.valueGoodsAndServicesSold) &&
                Objects.equals(otherIncome, that.otherIncome) &&
                Objects.equals(totalIncome, that.totalIncome) &&
                Objects.equals(payInCash, that.payInCash) &&
                Objects.equals(otherExpenses, that.otherExpenses) &&
                Objects.equals(totalExpenses, that.totalExpenses) &&
                Objects.equals(comments, that.comments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recid, description, valueGoodsAndServicesSold, otherIncome, totalIncome, payInCash, otherExpenses, totalExpenses, comments);
    }

    @OneToOne(mappedBy = "invoiceIncomeAndExpenses")
    @JsonIgnore
    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoicesByCostRecid) {
        this.invoice = invoicesByCostRecid;
    }
}
