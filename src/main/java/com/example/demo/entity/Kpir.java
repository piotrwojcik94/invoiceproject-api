package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "kpir", schema = "invoiceapp")
public class Kpir {
    private int recid;
    private Date date;
    private Collection<Invoice> invoices;
    private InvoiceIncomeAndExpenses incomeAndExpensesCurrentPage;
    private InvoiceIncomeAndExpenses incomeAndExpensesPreviousPage;
    private InvoiceIncomeAndExpenses incomeAndExpensesYear;
    private User userByKpirUser;
    private Double taxToPay;

    public Kpir() {}

    public Kpir(Date date, InvoiceIncomeAndExpenses page, InvoiceIncomeAndExpenses previousPage, InvoiceIncomeAndExpenses yearPage, User user, Double taxToPay) {
        this.date = date;
        this.invoices = new ArrayList<>();
        this.incomeAndExpensesCurrentPage = page;
        this.incomeAndExpensesPreviousPage = previousPage;
        this.incomeAndExpensesYear = yearPage;
        this.userByKpirUser = user;
        this.taxToPay = taxToPay;
    }

    @Id
    @Column(name = "kpir_recid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRecid() {
        return recid;
    }

    public void setRecid(int kpirRecid) {
        this.recid = kpirRecid;
    }

    @Basic
    @Column(name = "kpir_date")
    public Date getDate() {
        return date;
    }

    public void setDate(Date kpirDate) {
        this.date = kpirDate;
    }

    @Basic
    @JsonProperty("taxToPay")
    @Column(name = "kpir_tax_to_pay")
    public Double getKpirTaxToPay() {
        return taxToPay;
    }

    public void setKpirTaxToPay(Double kpirTaxToPay) {
        this.taxToPay = kpirTaxToPay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kpir kpir = (Kpir) o;
        return recid == kpir.recid &&
                Objects.equals(date, kpir.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recid, date);
    }

    @OneToMany(mappedBy = "kpir", fetch=FetchType.EAGER, cascade =  CascadeType.MERGE)
    @OrderBy("delivery")
    public Collection<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(Collection<Invoice> invoicesByKpirRecid) {
        this.invoices = invoicesByKpirRecid;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "kpir_page", referencedColumnName = "cost_recid")
    public InvoiceIncomeAndExpenses getIncomeAndExpensesCurrentPage() {
        return incomeAndExpensesCurrentPage;
    }

    public void setIncomeAndExpensesCurrentPage(InvoiceIncomeAndExpenses invoiceIncomeAndExpensesByKpirPage) {
        this.incomeAndExpensesCurrentPage = invoiceIncomeAndExpensesByKpirPage;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "kpir_previous_page", referencedColumnName = "cost_recid")
    public InvoiceIncomeAndExpenses getIncomeAndExpensesPreviousPage() {
        return incomeAndExpensesPreviousPage;
    }

    public void setIncomeAndExpensesPreviousPage(InvoiceIncomeAndExpenses invoiceIncomeAndExpensesByKpirPreviousPage) {
        this.incomeAndExpensesPreviousPage = invoiceIncomeAndExpensesByKpirPreviousPage;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "kpir_from_year", referencedColumnName = "cost_recid")
    public InvoiceIncomeAndExpenses getIncomeAndExpensesYear() {
        return incomeAndExpensesYear;
    }

    public void setIncomeAndExpensesYear(InvoiceIncomeAndExpenses invoiceIncomeAndExpensesByKpirFromYear) {
        this.incomeAndExpensesYear = invoiceIncomeAndExpensesByKpirFromYear;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "kpir_user", referencedColumnName = "user_recid")
    public User getUserByKpirUser() {
        return userByKpirUser;
    }

    public void setUserByKpirUser(User userByKpirUser) {
        this.userByKpirUser = userByKpirUser;
    }
}
