package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "invoice_item", schema = "invoiceapp")
public class InvoiceItem {
    private int recid;
    private String name;
    private double amount;
    private String unit;
    private int vat;
    private double priceVat;
    private double priceNet;
    private double priceGross;
    private double priceAmountNet;
    private double priceAmountGross;
    private TaxationReason taxationReason;
    private Boolean deduct50;
    private String description;
    private Invoice invoice;

    @Id
    @Column(name = "invoice_item_recid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRecid() {
        return recid;
    }

    public void setRecid(int invoiceItemRecid) {
        this.recid = invoiceItemRecid;
    }

    @Basic
    @Column(name = "invoice_item_name")
    public String getName() {
        return name;
    }

    public void setName(String invoiceItemName) {
        this.name = invoiceItemName;
    }

    @Basic
    @Column(name = "invoice_item_amount")
    public double getAmount() {
        return amount;
    }

    public void setAmount(double invoiceItemAmount) {
        this.amount = invoiceItemAmount;
    }

    @Basic
    @Column(name = "invoice_item_unit")
    public String getUnit() {
        return unit;
    }

    public void setUnit(String invoiceItemUnit) {
        this.unit = invoiceItemUnit;
    }

    @Basic
    @Column(name = "invoice_item_vat")
    public int getVat() {
        return vat;
    }

    public void setVat(int invoiceItemVat) {
        this.vat = invoiceItemVat;
    }

    @Basic
    @Column(name = "invoice_item_price_vat")
    public double getPriceVat() {
        return priceVat;
    }

    public void setPriceVat(double invoiceItemPriceVat) {
        this.priceVat = invoiceItemPriceVat;
    }

    @Basic
    @Column(name = "invoice_item_price_net")
    public double getPriceNet() {
        return priceNet;
    }

    public void setPriceNet(double invoiceItemPriceNet) {
        this.priceNet = invoiceItemPriceNet;
    }

    @Basic
    @Column(name = "invoice_item_price_gross")
    public double getPriceGross() {
        return priceGross;
    }

    public void setPriceGross(double invoiceItemPriceGross) {
        this.priceGross = invoiceItemPriceGross;
    }

    @Basic
    @Column(name = "invoice_item_price_amount_net")
    public double getPriceAmountNet() {
        return priceAmountNet;
    }

    public void setPriceAmountNet(double invoiceItemPriceAmountNet) {
        this.priceAmountNet = invoiceItemPriceAmountNet;
    }

    @Basic
    @Column(name = "invoice_item_price_amount_gross")
    public double getPriceAmountGross() {
        return priceAmountGross;
    }

    public void setPriceAmountGross(double invoiceItemPriceAmountGross) {
        this.priceAmountGross = invoiceItemPriceAmountGross;
    }

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "invoice_item_taxation_reason")
    public TaxationReason getTaxationReason() {
        return taxationReason;
    }

    public void setTaxationReason(TaxationReason invoiceItemTaxationReason) {
        this.taxationReason = invoiceItemTaxationReason;
    }

    @Basic
    @Column(name = "invoice_item_deduct50")
    public Boolean getDeduct50() {
        return deduct50;
    }

    public void setDeduct50(Boolean invoiceItemDeduct50) {
        this.deduct50 = invoiceItemDeduct50;
    }

    @Basic
    @Column(name = "invoice_item_description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String invoiceItemDescription) {
        this.description = invoiceItemDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvoiceItem that = (InvoiceItem) o;
        return recid == that.recid &&
                Double.compare(that.amount, amount) == 0 &&
                vat == that.vat &&
                Double.compare(that.priceVat, priceVat) == 0 &&
                Double.compare(that.priceNet, priceNet) == 0 &&
                Double.compare(that.priceGross, priceGross) == 0 &&
                Double.compare(that.priceAmountNet, priceAmountNet) == 0 &&
                Double.compare(that.priceAmountGross, priceAmountGross) == 0 &&
                Objects.equals(name, that.name) &&
                Objects.equals(unit, that.unit) &&
                Objects.equals(taxationReason, that.taxationReason) &&
                Objects.equals(deduct50, that.deduct50) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recid, name, amount, unit, vat, priceVat, priceNet, priceGross, priceAmountNet, priceAmountGross, taxationReason, deduct50, description);
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "invoice_item_invoice", referencedColumnName = "invoice_recid")
    @JsonIgnore
    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoiceByInvoiceItemInvoice) {
        this.invoice = invoiceByInvoiceItemInvoice;
    }
}
