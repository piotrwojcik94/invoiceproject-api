package com.example.demo.entity;

public enum InvoiceType {
    sale("0"), purchase("1"), no_vat("2");

    private final String value;

    InvoiceType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
