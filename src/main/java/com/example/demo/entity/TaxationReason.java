package com.example.demo.entity;

public enum TaxationReason {
    other_goods_and_services("0"), permanently_resources("1");

    private String value;

    TaxationReason(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
