package com.example.demo.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class AuthenticationTokenFilter extends GenericFilterBean {

    private AuthenticationTokenProvider authenticationTokenProvider;

    public AuthenticationTokenFilter(AuthenticationTokenProvider authenticationTokenProvider) {
        this.authenticationTokenProvider = authenticationTokenProvider;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String authenticationHeader = ((HttpServletRequest)servletRequest).getHeader(SecurityGlobals.HEADER_FIELD_NAME);
        String token = authenticationTokenProvider.getTokenFromHTTPHeader(authenticationHeader);

        if (authenticationTokenProvider.validateToken(token)) {
            Authentication auth = authenticationTokenProvider.getAuthentication(token);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
