package com.example.demo.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class AuthenticationTokenProvider {

    @Autowired
    private UserDetailsService userDetailsService;


    public String createToken(UserDetails userDetails, boolean futureToken) {
        Date now = new Date();
        Date validity = futureToken ? new Date(now.getTime() + SecurityGlobals.TIME_TO_LIVE * 2) : new Date(now.getTime() + SecurityGlobals.TIME_TO_LIVE);
        Date validFrom = futureToken ? new Date(now.getTime() + SecurityGlobals.TIME_TO_LIVE / 2) : now;

        Claims claims = Jwts.claims()
                .setSubject(userDetails.getUsername())
                .setIssuer(SecurityGlobals.ISSUER)
                .setIssuedAt(now)
                .setNotBefore(validFrom)
                .setExpiration(validity);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SecurityGlobals.SIGNING_KEY)
                .compact();
    }

    public Authentication getAuthentication(String token) {
        UserDetails userDetails = null;
        UsernamePasswordAuthenticationToken upat = null;
        String username = getUsername(token);

        try {
            userDetails = userDetailsService.loadUserByUsername(username);
            upat = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
        } catch (UsernameNotFoundException e) {
            // User not found (username may have been null)
        }

        return upat;
    }

    public String getUsername(String token) {
        String username = "";

        username =  Jwts.parser()
                .setSigningKey(SecurityGlobals.SIGNING_KEY)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();

        return username;
    }

    public String getTokenFromHTTPHeader(String authorizationHeader) {
        if (authorizationHeader != null && authorizationHeader.startsWith(SecurityGlobals.TOKEN_PREFIX)) {
            return authorizationHeader.substring(SecurityGlobals.TOKEN_PREFIX.length());
        }
        return null;
    }

    public boolean validateToken(String token) {
        if(null == token)
            return false;

        Jws<Claims> claims = Jwts.parser().setSigningKey(SecurityGlobals.SIGNING_KEY).parseClaimsJws(token);

        return !claims.getBody().getExpiration().before(new Date());
    }
}
