package com.example.demo.security;

import org.springframework.security.core.context.SecurityContextHolder;

public class AuthenticationManager {

    public static String getCurrentUser() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
