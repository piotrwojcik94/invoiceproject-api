package com.example.demo.security;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;

public final class SecurityGlobals {
    public static final String HEADER_FIELD_NAME = "Authorization";
    public static final String TOKEN_PREFIX = "PW";
    public static final String ROLE_PREFIX = "";
    public static final String ISSUER = "CONTROL_CENTER_BACKEND";
    public static final Long TIME_TO_LIVE = 300000L; // 5 minutes
    public static final SecretKey SIGNING_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);
}
