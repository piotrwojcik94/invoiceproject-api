package com.example.demo.security;

public class AuthenticationResponse {
    private String token, futureToken;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFutureToken() {
        return futureToken;
    }

    public void setFutureToken(String futureToken) {
        this.futureToken = futureToken;
    }
}
