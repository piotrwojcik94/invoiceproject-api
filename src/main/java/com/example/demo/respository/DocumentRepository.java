package com.example.demo.respository;

import com.example.demo.entity.Document;
import org.springframework.data.repository.Repository;

public interface DocumentRepository extends Repository<Document, Integer> {
    void save(Document document);

    Document findByRecid(int recid);
}
