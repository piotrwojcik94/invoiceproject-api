package com.example.demo.respository;

import com.example.demo.entity.Invoice;
import com.example.demo.entity.InvoiceType;
import com.example.demo.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;


import java.util.Date;
import java.util.List;

public interface InvoiceRepository extends Repository<Invoice, Integer> {

    List<Invoice> findAllByKpir_Recid(int recid);

    @Query("select i from Invoice i where i.issue >= ?1 and i.issue <= ?2 and i.user = ?3 order by i.invoiceType, i.posting")
    List<Invoice> findAllByDelivery(Date startDate, Date endDate, User user);

    @Query("select i from Invoice i where i.issue >= ?1 and i.issue <= ?2 and i.kpir is null and i.user = ?3 order by i.invoiceType, i.posting")
    List<Invoice> findAllByDeliveryAndNotInKpir(Date startDate, Date endDate, User user);

    @Query("select i from Invoice i where i.issue >= ?1 and i.issue <= ?2 and i.invoiceType in ?3 and i.user = ?4  order by i.posting")
    List<Invoice> findAllByDeliveryAndInvoiceType(Date startDate, Date endDate, List<InvoiceType> invoiceType, User user);

    @Query("select i from Invoice i where i.issue >= ?1 and i.issue <= ?2 and i.user = ?3  order by i.issue, i.invoiceType")
    List<Invoice> findAllByYear(Date startDate, Date endDate, User user);

    Invoice findByRecid(int recid);

    void delete(Invoice invoice);

    void save(Invoice invoice);
}
