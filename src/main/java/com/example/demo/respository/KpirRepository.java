package com.example.demo.respository;

import com.example.demo.entity.Kpir;
import com.example.demo.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.Date;
import java.util.List;

public interface KpirRepository extends Repository<Kpir, Integer> {

    @Query("select k from Kpir k where k.date >= ?1 and k.date <= ?2 and k.userByKpirUser = ?3 order by k.date asc")
    List<Kpir> findKpirByYear(Date startDate, Date endDate, User user);

    @Query("select k from Kpir k where k.date >= ?1 and k.date <= ?2 and k.userByKpirUser = ?3 order by k.date asc")
    Kpir findKpirByKpirDate(Date startDate, Date endDate, User user);

    Kpir save(Kpir kpir);
}
