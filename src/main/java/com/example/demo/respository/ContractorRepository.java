package com.example.demo.respository;

import com.example.demo.entity.Contractor;
import com.example.demo.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface ContractorRepository extends Repository<Contractor, Integer> {

    @Query("select c from Contractor c where c.recid = :id")
    Contractor findById(int id);

    void save(Contractor contractor);

    Page<Contractor> getAllByContractorUserAndNameContainingOrNipContaining(User user, String name, String nip, Pageable pageable);

    @Query("select c from Contractor c where c.nip like :firstNipNumbers%")
    List<Contractor> getSuggestions(String firstNipNumbers);
}
