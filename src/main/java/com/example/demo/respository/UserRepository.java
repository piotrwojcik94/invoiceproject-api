package com.example.demo.respository;

import com.example.demo.entity.User;
import org.springframework.data.repository.Repository;

public interface UserRepository extends Repository<User, Integer> {
    void save(User user);

    User getUserByLogin(String userLogin);

    User getUserByNip(String userNip);
}
