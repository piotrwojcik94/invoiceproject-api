package com.example.demo.statisticCalculator;

import com.example.demo.entity.Kpir;
import com.example.demo.tools.Const;
import com.example.demo.tools.DateHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class IncomeAndExpenseMonthlyAscending extends IncomeAndExpenseCalculator {
    double totalIncome;
    double totalExpense;

    public IncomeAndExpenseMonthlyAscending(List<Kpir> kpirs) {
        this.kpirs = kpirs;
        this.totalIncome = 0;
        this.totalExpense = 0;
    }

    @Override
    public HashMap<String, List<Double>> calculate() {
        for (Kpir kpir: kpirs) {
            values = new ArrayList<>();
            totalIncome += kpir.getIncomeAndExpensesCurrentPage().getTotalIncome();
            totalExpense += kpir.getIncomeAndExpensesCurrentPage().getTotalExpenses();
            values.add(totalIncome);
            values.add(totalExpense);
            statistic.put(Const.POLISH_MONTHS[DateHelper.getMonthNumber(kpir.getDate())-1], values);
        }
        return statistic;
    }
}
