package com.example.demo.statisticCalculator;

import com.example.demo.entity.Kpir;
import com.example.demo.tools.Const;
import com.example.demo.tools.DateHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class IncomeAndExpenseMonthly extends IncomeAndExpenseCalculator {

    public IncomeAndExpenseMonthly(List<Kpir> kpirs) {
        this.kpirs = kpirs;
    }
    @Override
    public HashMap<String, List<Double>> calculate() {
        for (Kpir kpir: kpirs) {
            values = new ArrayList<>();
            values.add(kpir.getIncomeAndExpensesCurrentPage().getTotalIncome());
            values.add(kpir.getIncomeAndExpensesCurrentPage().getTotalExpenses());
            statistic.put(Const.POLISH_MONTHS[DateHelper.getMonthNumber(kpir.getDate())-1], values);
        }
        return statistic;
    }
}
