package com.example.demo.statisticCalculator;

import com.example.demo.entity.Invoice;
import com.example.demo.tools.Const;
import com.example.demo.tools.DateHelper;

import java.util.HashMap;
import java.util.List;

public class VatCalculatorMonthly extends VatCalculator {

    public VatCalculatorMonthly(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    @Override
    public HashMap<String, List<Double>> calculate() {
        for(Invoice invoice: invoices) {
            int month = DateHelper.getMonthNumber(invoice.getIssue());
            this.calculateVatFromInvoice(invoice);
            if(statistic.containsKey(Const.POLISH_MONTHS[month-1])) {
                updateMonth(month);
            } else {
                addNewMonth(month);
            }
        }
        return statistic;
    }
}