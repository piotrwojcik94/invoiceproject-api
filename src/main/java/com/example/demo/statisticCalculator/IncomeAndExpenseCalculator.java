package com.example.demo.statisticCalculator;

import com.example.demo.entity.Kpir;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public abstract class IncomeAndExpenseCalculator {
    protected List<Kpir> kpirs;
    protected HashMap<String, List<Double>> statistic;
    protected List<Double> values;
    public abstract HashMap<String, List<Double>> calculate();

    public IncomeAndExpenseCalculator() {
        this.statistic = new LinkedHashMap<>();
    }
}
