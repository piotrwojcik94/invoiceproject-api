package com.example.demo.statisticCalculator;

import com.example.demo.entity.Invoice;
import com.example.demo.entity.InvoiceItem;
import com.example.demo.entity.InvoiceType;
import com.example.demo.tools.Const;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public abstract class VatCalculator {
    protected List<Invoice> invoices;
    protected List<Double> values;
    protected double vatOnGoods;
    protected double vatCounted;
    protected HashMap<String, List<Double>> statistic;

    public abstract HashMap<String, List<Double>> calculate();

    protected VatCalculator() {
        this.statistic = new LinkedHashMap<>();
    }

    protected void calculateVatFromInvoice(Invoice invoice) {
        this.vatOnGoods = 0;
        this.vatCounted = 0;
        if (InvoiceType.sale.equals(invoice.getInvoiceType())) {
            vatOnGoods += invoice.getVat();
        } else {
            for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {
                vatCounted += invoiceItem.getDeduct50() ? 0.5 * invoiceItem.getPriceVat() : invoiceItem.getPriceVat();
            }
        }
    }

    protected void addNewMonth(int month) {
        values = new ArrayList<>();
        values.add(0, vatOnGoods);
        values.add(1, vatCounted);
        values.add(2, vatOnGoods - vatCounted);
        statistic.put(Const.POLISH_MONTHS[month - 1], values);
    }

    protected void updateMonth(int month) {
        values = statistic.get(Const.POLISH_MONTHS[month - 1]);
        values.set(0, values.get(0) + vatOnGoods);
        values.set(1, values.get(1) + vatCounted);
        values.set(2, values.get(2) + vatOnGoods - vatCounted);
    }
}
