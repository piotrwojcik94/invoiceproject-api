package com.example.demo.statisticCalculator;

import com.example.demo.entity.Invoice;
import com.example.demo.tools.Const;
import com.example.demo.tools.DateHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VatCalculatorMonthlyAscending extends VatCalculator {

    public VatCalculatorMonthlyAscending(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    @Override
    public HashMap<String, List<Double>> calculate() {
        for(Invoice invoice: invoices) {
            int month = DateHelper.getMonthNumber(invoice.getIssue());
            this.calculateVatFromInvoice(invoice);
            if(statistic.containsKey(Const.POLISH_MONTHS[month-1])) {
                updateMonth(month);
            } else {
                if(month > 1 && statistic.containsKey(Const.POLISH_MONTHS[month-2])) {
                    addNewMonthWithPreviousMonthValues(month);
                } else {
                    addNewMonth(month);
                }
            }
        }
        return statistic;
    }

    private void addNewMonthWithPreviousMonthValues(int month) {
        values = new ArrayList<>();
        List<Double> prevMonthValues = statistic.get(Const.POLISH_MONTHS[month - 2]);
        values.add(0, prevMonthValues.get(0) + vatOnGoods);
        values.add(1, prevMonthValues.get(1) + vatCounted);
        values.add(2, prevMonthValues.get(2) + vatOnGoods - vatCounted);
        statistic.put(Const.POLISH_MONTHS[month - 1], values);
    }
}
