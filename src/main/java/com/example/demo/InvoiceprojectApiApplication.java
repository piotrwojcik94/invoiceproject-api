package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.example.demo.respository")
@Configuration
@SpringBootApplication
public class InvoiceprojectApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(InvoiceprojectApiApplication.class, args);
    }

}
