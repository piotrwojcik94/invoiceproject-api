package com.example.demo.controller;

import com.example.demo.entity.Invoice;
import com.example.demo.entity.Kpir;
import com.example.demo.respository.UserRepository;
import com.example.demo.security.AuthenticationManager;
import com.example.demo.service.InvoiceServiceImpl;
import com.example.demo.service.KpirServiceImpl;
import com.example.demo.statisticCalculator.IncomeAndExpenseMonthly;
import com.example.demo.statisticCalculator.IncomeAndExpenseMonthlyAscending;
import com.example.demo.statisticCalculator.VatCalculatorMonthly;
import com.example.demo.statisticCalculator.VatCalculatorMonthlyAscending;
import com.example.demo.tools.FindBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/Statistic")
public class StatisticController {

    @Autowired
    private KpirServiceImpl kpirService;

    @Autowired
    private InvoiceServiceImpl invoiceService;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(path = "/incomeAndExpense", method = RequestMethod.GET)
    public HashMap<String, List<Double>> incomeAndExpense(@RequestParam("year") int year) {
        List<Kpir> kpirs = this.getKpirs(year);
        return new IncomeAndExpenseMonthly(kpirs).calculate();
    }

    @RequestMapping(path = "/incomeAndExpenseAscending", method = RequestMethod.GET)
    public HashMap<String, List<Double>> incomeAndExpenseAscending(@RequestParam("year") int year) {
        List<Kpir> kpirs = this.getKpirs(year);
        return new IncomeAndExpenseMonthlyAscending(kpirs).calculate();
    }

    @RequestMapping(path = "/vat", method = RequestMethod.GET)
    public HashMap<String, List<Double>> getInvoicesByYear(@RequestParam("year") int year) {
        List<Invoice> invoices = this.getInvoices(year);
        return new VatCalculatorMonthly(invoices).calculate();
    }

    @RequestMapping(path = "/vatAscending", method = RequestMethod.GET)
    public HashMap<String, List<Double>> getInvoicesByYearAscending(@RequestParam("year") int year) {
        List<Invoice> invoices = this.getInvoices(year);
        return new VatCalculatorMonthlyAscending(invoices).calculate();
    }

    private List<Kpir> getKpirs(int year) {
        FindBuilder builder = new FindBuilder();
        builder.setDatesYear(year)
                .setUser(userRepository.getUserByLogin(AuthenticationManager.getCurrentUser()));
        return kpirService.findKpirByYear(builder.build());
    }

    private List<Invoice> getInvoices(int year) {
        FindBuilder builder = new FindBuilder();
        builder.setDatesYear(year)
                .setUser(userRepository.getUserByLogin(AuthenticationManager.getCurrentUser()));
        return invoiceService.findAllByYear(builder.build());
    }
}
