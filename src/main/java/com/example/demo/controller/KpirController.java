package com.example.demo.controller;

import com.example.demo.entity.Kpir;
import com.example.demo.entity.KpirAndInvoicesDto;
import com.example.demo.entity.KpirDto;
import com.example.demo.respository.UserRepository;
import com.example.demo.security.AuthenticationManager;
import com.example.demo.service.KpirServiceImpl;
import com.example.demo.tools.FindBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/Invoice")
public class KpirController {

    @Autowired
    private KpirServiceImpl kpirService;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(path = "/getKpir", method = RequestMethod.GET)
    public Kpir findKpirByDate(@RequestParam("year") int year, @RequestParam("month") int month) {
        FindBuilder builder = new FindBuilder();
        builder.setDates(year, month)
                .setUser(userRepository.getUserByLogin(AuthenticationManager.getCurrentUser()));
        return kpirService.findKpirByDate(builder.build());
    }

    @RequestMapping(path = "/getKpirAndInvoices", method = RequestMethod.GET)
    public KpirAndInvoicesDto findKpirAndInvoices(@RequestParam("year") int year, @RequestParam("month") int month) {
        FindBuilder builder = new FindBuilder();
        builder.setDates(year, month)
                .setUser(userRepository.getUserByLogin(AuthenticationManager.getCurrentUser()));
        return kpirService.findKpirAndInvoices(builder.build());
    }

    @RequestMapping(path = "/getKpirByYear", method = RequestMethod.GET)
    public List getKpirByYear(@RequestParam("year") int year) {
        FindBuilder builder = new FindBuilder();
        builder.setDatesYear(year)
                .setUser(userRepository.getUserByLogin(AuthenticationManager.getCurrentUser()));
        return kpirService.findKpirByYear(builder.build());
    }

    @PostMapping(path = "/kpir")
    public Kpir saveKpir(@RequestBody KpirDto kpir) {
        return kpirService.saveAndFlush(kpir);
    }
}
