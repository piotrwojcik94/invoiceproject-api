package com.example.demo.controller;

import com.example.demo.entity.InvoiceItem;
import com.example.demo.entity.TaxationForm;
import com.example.demo.entity.User;
import com.example.demo.entity.UserDto;
import com.example.demo.security.AuthenticationRequest;
import com.example.demo.security.AuthenticationResponse;
import com.example.demo.security.AuthenticationTokenProvider;
import com.example.demo.security.SecurityGlobals;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "user")
public class UserController {

    @Autowired
    AuthenticationTokenProvider authenticationTokenProvider;

    @Autowired
    private UserService userService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping(path = "/newUser")
    public void saveUser(@RequestBody() UserDto user) {
        userService.save(user.user);
    }

    @PostMapping(path = "/updateUser")
    public void updateUser(@RequestBody() UserDto user) {
        user.user.setTaxationForms(user.taxationForms);
        for (TaxationForm item : user.taxationForms)
            item.setUserByTaxationFormUserRecid(user.user);
        userService.update(user.user);
    }

    @RequestMapping(path = "/getUserByUserLogin")
    public User getUserByUserLogin(@RequestParam("login") String userLogin) {
        return userService.getUserByLogin(userLogin);
    }

    @RequestMapping(path = "/isEmailUnique")
    public boolean isEmailUnique(@RequestParam("login") String email) {
        return userService.isEmailUnique(email);
    }

    @RequestMapping(path = "/getUserByUserNIP")
    public User getUserByUserNIP(@RequestParam("nip") String userNIP) {
        return userService.getUserByNIP(userNIP);
    }

    @RequestMapping(path = "/isNIPUnique")
    public boolean isNIPUnique(@RequestParam("nip") String userNIP) {
        return userService.isNIPUnique(userNIP);
    }


    @RequestMapping(path = "/getUserNIPByLogin")
    public String getUserNIPByLogin(@RequestParam("login") String userLogin) {
        return userService.getUserByNIP(userLogin).getNip();
    }

    @PostMapping("/login")
    public AuthenticationResponse user(@RequestBody AuthenticationRequest data) {
        AuthenticationResponse response = new AuthenticationResponse();

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(data.getUsername(), data.getPassword()));
        UserDetails user = userDetailsService.loadUserByUsername(data.getUsername());

        if (user!= null) {
            response.setToken(authenticationTokenProvider.createToken(user, false));
            response.setFutureToken(authenticationTokenProvider.createToken(user, true));
        }
        return response;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public AuthenticationResponse renewToken(@RequestHeader(SecurityGlobals.HEADER_FIELD_NAME) String authorization) {
        String token = authenticationTokenProvider.getTokenFromHTTPHeader(authorization);
        String userName = authenticationTokenProvider.getUsername(token);

        AuthenticationResponse response = new AuthenticationResponse();

        try {
            UserDetails user = userDetailsService.loadUserByUsername(userName);

            response.setToken(authenticationTokenProvider.createToken(user, false));
            response.setFutureToken(authenticationTokenProvider.createToken(user, true));
        } catch (Exception e) {

        }

        return response;
    }
}
