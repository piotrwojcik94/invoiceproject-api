package com.example.demo.controller;

import com.example.demo.entity.Document;
import com.example.demo.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/document")
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @PostMapping(path = "/upload/db")
    public ResponseEntity uploadToDB(@RequestParam("file") MultipartFile file) {
//        Document doc = new Document();
//        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//        doc.setDocumentName(fileName);
//        try {
//            //doc.setDocumentFile(file.getBytes());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        documentService.save(doc);
//        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/files/download/")
//                .path(fileName).path("/db")
//                .toUriString();
        return ResponseEntity.ok(null);
    }

    @GetMapping("/download/{id}")
    public ResponseEntity downloadFromDB(@PathVariable("id") int id) {
        String contentType = "application/octet-stream";
        Document document = documentService.findByRecid(id);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + document.getName() + "\"")
                .body(document.getFile());
    }
}
