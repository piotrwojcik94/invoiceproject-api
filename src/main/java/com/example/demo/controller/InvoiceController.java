package com.example.demo.controller;

import com.example.demo.entity.Document;
import com.example.demo.entity.Invoice;
import com.example.demo.entity.InvoiceDto;
import com.example.demo.entity.InvoiceType;
import com.example.demo.respository.UserRepository;
import com.example.demo.security.AuthenticationManager;
import com.example.demo.service.DocumentService;
import com.example.demo.service.InvoiceServiceImpl;
import com.example.demo.tools.FindBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/Invoice")
public class InvoiceController {

    @Autowired
    private InvoiceServiceImpl invoiceService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DocumentService documentService;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public List findAllInvoices(@RequestParam("year") int year, @RequestParam("month") int month, @RequestParam("invoiceType") List<InvoiceType> invoiceType) {
        FindBuilder builder = new FindBuilder();
        builder.setDates(year, month)
                .setUser(userRepository.getUserByLogin(AuthenticationManager.getCurrentUser()))
                .setInvoiceType(invoiceType);
        return invoiceService.findAllByDeliveryAndInvoiceType(builder.build());
    }

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public List findAllInvoices(@RequestParam("year") int year, @RequestParam("month") int month) {
        FindBuilder builder = new FindBuilder();
        builder.setDates(year, month)
                .setUser(userRepository.getUserByLogin(AuthenticationManager.getCurrentUser()));
        return invoiceService.findAllByDelivery(builder.build());
    }

    @PostMapping(path = "/newInvoice")
    public void savePurchaseInvoice(@RequestParam("invoice") String invoice, @RequestParam(name = "file", required = false) MultipartFile file, @RequestParam(name = "documentId", required = false) String documentId) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Invoice invoice1 = objectMapper.readValue(invoice, Invoice.class);
            if (documentId != null) {
                int docId = Integer.parseInt(documentId);
                if (docId > 0) {
                    Document document = documentService.findByRecid(docId);
                    invoice1.setDocument(document);
                }
            }
            if (file != null) {
                Document doc = new Document(file);
                invoice1.setDocument(doc);
            }
            invoiceService.saveAndFlush(invoice1, invoice1.getInvoiceItems());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @DeleteMapping(path = {"/deleteInvoice/{id}"})
    public Invoice deletePurchaseInvoice(@PathVariable("id") int id) {
        return invoiceService.delete(id);
    }

    @PostMapping(path = "/updateInvoice")
    public void updatePurchaseInvoice(@RequestBody() InvoiceDto invoiceDto) {
        invoiceService.saveAndFlush(invoiceDto.invoice, invoiceDto.invoiceItems);
    }
}
