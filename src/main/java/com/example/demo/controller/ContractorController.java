package com.example.demo.controller;

import com.example.demo.entity.Contractor;
import com.example.demo.service.ContractorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "/contractor")
public class ContractorController {

    @Autowired
    private ContractorService contractorService;

    @RequestMapping(path = "/contractors")
    public Page<Contractor> getContractors(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            @RequestParam(value = "filter", defaultValue = "") String filter) {
        return contractorService.getContractors(page, size, filter);
    }

    @PutMapping(path = "/update/{id}")
    public void updateContractor(
            @PathVariable int id,
            @RequestBody Contractor contractor) {
        contractorService.update(id, contractor);
    }

    @GetMapping("/suggestions")
    public ResponseEntity<List<Contractor>> getSuggestions(@RequestParam String firstNipNumber) {
        List<Contractor> suggestions = contractorService.getSuggestions(firstNipNumber);
        return ResponseEntity.ok(suggestions);
    }
}
